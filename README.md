Each student has to fork this repository, add a file with their name and containing their Gitlab.com username and prefered contact email address then create a merge request to get their information added to the original repository.

Check out the *John Doe* file for a template of what is required.
